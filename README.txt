# syms2elf_ghidra

A script for Ghidra to export the symbols recognized to the ELF symbol table


dependencies:
python3

pip install ghidra_bridge
python -m ghidra_bridge.install_server ~/ghidra_scripts

copy syms2elf* scripts to ~/ghidra_scripts

